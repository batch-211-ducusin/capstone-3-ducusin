import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext.js';
import Swal from 'sweetalert2';

export default function Register() {

	const { user } = useContext(UserContext);
	const navigate = useNavigate();

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [isActive, setIsActive] = useState('');

	useEffect(()=> {
		if(
			(
			firstName !== "" &&
			lastName !== "" &&
			mobileNo.length === 11 &&
			email !== "" &&
			password1 !== "" &&
			password2 !== ""
			)
			&&
			(password1 === password2)
		) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	},[firstName, lastName, email, mobileNo, password1, password2]);

	function registerUser(e) {
		e.preventDefault();
		fetch(`${process.env.REACT_APP_URL}/users/checkEmail`, {
			method: 'POST',
			headers: {'Content-Type':'application/json'},
			body: JSON.stringify({email: email})
		})
		.then(res=> res.json())
		.then(data=> {
			if(data === true) {
				Swal.fire({
					title: "Dupicate Email Found!",
					icon: "error",
					text: "Please use another email."
				})
			} else {
				fetch(`${process.env.REACT_APP_URL}/users/register`, {
					method: 'POST',
					headers: {'Content-Type':'application/json',},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						password: password1
					})
				})
				.then(res=> res.json())
				.then(res=> {
						setFirstName("");
						setLastName("");
						setEmail("");
						setMobileNo("");
						setPassword1("");
						setPassword2("");

						Swal.fire({
							title: "Registration successfull!",
							icon: "success",
							text: "Welcome to Inuman Express!"
						})
						
						navigate("/login")
					// <Navigate to='/login'/>
				})
			}
		})
	}

	return (
		(user.id !== null)
		?
		<Navigate to="/products" />
		:
		<div className="container mt-5">
		<div className="row justify-content-center">
		<div className="col-6">
		<h1 className="text-center">Register</h1>
		<Form onSubmit={(e)=> registerUser(e)}>

			<Form.Group className="my-3" controlId="firstName">
				<Form.Label>
					First Name
				</Form.Label>
				<Form.Control
					type="text"
					placeholder="First Name"
					value={firstName}
					onChange={e=> setFirstName(e.target.value)}
					required
					>
				</Form.Control>
			</Form.Group>

			<Form.Group className="mb-3" controlId="lastName">
				<Form.Label>
					Last Name
				</Form.Label>
				<Form.Control
					type="text"
					placeholder="Last Name"
					value={lastName}
					onChange={e=> setLastName(e.target.value)}
					required
					>
				</Form.Control>
			</Form.Group>

			<Form.Group className="mb-3" controlId="mobileNo">
				<Form.Label>
					Mobile Number
				</Form.Label>
				<Form.Control
					type="number"
					placeholder="09XXXXXXXXX"
					value={mobileNo}
					onChange={e=> setMobileNo(e.target.value)}
					required
					>
				</Form.Control>
			</Form.Group>

			<Form.Group className="mb-3" controlId="userEmail">
				<Form.Label>
					Email address
				</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email"
					value={email}
					onChange={e=> setEmail(e.target.value)}
					required
					>
				</Form.Control>
			</Form.Group>

			<Form.Group className="mb-3" controlId="password1">
				<Form.Label>
					Password
				</Form.Label>
				<Form.Control
					type="password"
					placeholder="Password"
					value={password1}
					onChange={e=> setPassword1(e.target.value)}
					required
					>
				</Form.Control>
			</Form.Group>

			<Form.Group className="mb-3" controlId="password2">
				<Form.Label>
					Verify Password
				</Form.Label>
				<Form.Control
					type="password"
					placeholder="Verify Password"
					value={password2}
					onChange={e=> setPassword2(e.target.value)}
					required
					>
				</Form.Control>
			</Form.Group>

			{
				isActive
				?
				<Button
					variant="success"
					type="submit"
					id="submitBtn">
					Register
				</Button>
				:
				<Button
					variant="danger"
					type="submit"
					id="submitBtn"
					disabled>
					Register
				</Button>
			}
			
		</Form>
		</div>
		</div>
		</div>
	)
};