import { useState, useEffect, useContext } from 'react';
import ProductCard from '../components/ProductCard.js';
import UserContext from '../UserContext.js';

export default function Products() {

	const { user } = useContext(UserContext);

	const [products, setProducts] = useState([]);
	useEffect(()=> {
		fetch(`${process.env.REACT_APP_URL}/products/active`)
		.then(res=> res.json())
		.then(data=> {
			const productArr = (data.map(product=> {
				return(
					<ProductCard productProp={product} key={product._id}/>
				)
			}))
			setProducts(productArr);
		})
	},[products])

	return(
		<>
		<h1 className="text-center">Products</h1>
		{products}
		</>
	)
};