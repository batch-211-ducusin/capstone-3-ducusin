import Banner from '../components/Banner.js';

export default function Error() {
	const data = {
		title: "404 - Not Found",
		content: "page cannot be found.",
		destination: "/",
		label: "Go Back Home"
	}
	return(
		<Banner bannerProp={data}/>
	)
};