import Banner from '../components/Banner.js';
import Highlights from '../components/Highlights.js';

export default function Home() {
	const data = {
		title: 'Inuman Express!',
		content: "Have a hasle free party! leave the preparation to us!",
		destination: '/products',
		label: "Order Now!"
	}
	return(
		<>
			<Banner bannerProp={data}/>
			<Highlights/>
		</>
	)
};