import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { UserProvider } from './UserContext';
import { Container } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import ProductView from './components/ProductView.js';
import EditProduct from './components/EditProduct.js';
import AddProduct from './components/AddProduct.js';
import AdminDash from './components/AdminDash.js';
import AppNavbar from './components/AppNavbar.js';
import UserList from './components/UserList.js';
import Products from './pages/Products.js';
import Register from './pages/Register.js';
import Logout from './pages/Logout.js';
import Error from './pages/Error.js';
import Login from './pages/Login.js';
import Home from './pages/Home.js';


function App() {

  const [user, setUser] = useState({id:null, isAdmin:null});
  const unsetUser = ()=> {localStorage.clear()};

  useEffect(()=> {
    fetch(`${process.env.REACT_APP_URL}/users/details`, {
      headers: {Authorization:`Bearer ${localStorage.getItem('token')}`}
    })
    .then(res=> res.json())
    .then(data=> {
      console.log(data);
      if(typeof data._id !== "undefined") {
        setUser({id: data._id, isAdmin: data.isAdmin})
      } else {
        setUser({id: null, isAdmin: null})
      }
    })
  },[]);

  return(
    <div style={{ backgroundImage: "../public/images/background.jpg" }}>
    <UserProvider value={{user, setUser, unsetUser}}>
      <BrowserRouter>
        <AppNavbar/>
        <Container>
          <Routes>
            <Route path="/editProduct/:productId" element={<EditProduct/>} />
            <Route path="/products/:productId" element={<ProductView/>} />
            <Route path="/addProduct" element={<AddProduct/>} />
            {
              (user.isAdmin === true)
              ?
              <Route path="/AdminDash" element={<AdminDash/>} />
              :
              <>
              <Route path="/products" element={<Products/>} />
              </>
            }
            <Route path="/register" element={<Register/>} />
            <Route path="/userList" element={<UserList/>} />
            <Route path="/logout" element={<Logout/>} />
            <Route path="/login" element={<Login/>} />
            <Route path="/" element={<Home/>} />
            <Route path="*" element={<Error/>} />
          </Routes>
        </Container>
      </BrowserRouter>
    </UserProvider>
    </div>
  )
};

export default App;