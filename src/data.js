const data = {
	products: [
		{
			name: "Chicharong Bulaklak",
			slug: "chichraong-bulaklak",
			category: "Pulutan",
			image: "../public/images/1.jpg",
			price: 150,
			countInStock: 10,
			type: "Pork",
			rating: 4.5,
			numReviews: 10,
			description: "deep fried pork intestine"
		},
		{
			name: "Pritong Alagad",
			slug: "pritong-alagad",
			category: "Pulutan",
			image: "../public/images/2.jpg",
			price: 175,
			countInStock: 10,
			type: "Seafood",
			rating: 4.8,
			numReviews: 10,
			description: "deep baby crabs"
		}
	]
}

export default data;