import { Navbar, Nav, Container } from 'react-bootstrap';
import { useContext } from 'react';
import UserContext from '../UserContext.js';
import { Link } from 'react-router-dom';

export default function AppNavbar() {

    const { user } = useContext(UserContext)

    return(
        <Navbar bg="light" expand="lg">
            <Container>
                <Navbar.Brand as={Link} to="/">
                    Inuman Express!
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ms-auto">
                        {/*{
                            (user.isAdmin === true)
                            ?
                            <Nav.Link as={Link} to="/adminDash">Admin Dashboard</Nav.Link>
                            :
                            <>
                            <Nav.Link as={Link} to="/products">Products</Nav.Link>
                            </>
                        }
                        <Nav.Link as={Link} to="/">Home</Nav.Link>
                        {
                            (user.id !== null)
                            ?
                            <>
                            <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
                            <Nav.Link as={Link} to="/myTable">My Table</Nav.Link>
                            </>
                            :
                            <>
                            <Nav.Link as={Link} to="/login">Login</Nav.Link>
                            <Nav.Link as={Link} to="/register">Register</Nav.Link>
                            </>
                        }*/}
                        <Nav.Link as={Link} to="/">Home</Nav.Link>
                        {
                            (user.isAdmin === false)
                            ?
                            <>
                            <Nav.Link as={Link} to="/products">Products</Nav.Link>
                            <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
                            </>
                            :
                            <>
                            {
                                (user.isAdmin === true)
                                ?
                                <>
                                <Nav.Link as={Link} to="/adminDash">Admin Dashboard</Nav.Link>
                                <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
                                </>
                                :
                                <>
                                <Nav.Link as={Link} to="/register">Register</Nav.Link>
                                <Nav.Link as={Link} to="/login">Login</Nav.Link>
                                </>
                            }
                            </>
                        }
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
};

// (user.isAdmin === true)
// (user.id !== null)