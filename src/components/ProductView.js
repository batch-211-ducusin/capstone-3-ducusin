import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { Link, useParams, useNavigate } from 'react-router-dom';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext.js';
import Swal from 'sweetalert2';

export default function ProductView() {

	const navigate = useNavigate();
	const { productId } = useParams();
	const { user } = useContext(UserContext);
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");
	// const [currentUser, setCurrentUser] = useState("");

	// const retrieveUserDetails = (token)=> {
	// 	fetch('http://localhost:4000/users/details', {
	// 		headers: {Authorization: `Bearer ${token}`}
	// 	})
	// 	.then(res=> res.json())
	// 	.then(data=> {
	// 		console.log(data);
	// 		setUser({id: data._id, isAdmin: data.isAdmin});
	// 	})
	// }
	// setEmail('');
	// setPassword('');

	// function retrieveUserDetails(token) {
	// 	fetch('http://localhost:4000/users/details', {
	// 		headers: {Authorization: `Bearer ${token}`}
	// 	})
	// 	.then(res=> res.json())
	// 	.then(data=> {
	// 		console.log(data);
	// 		setCurrentUser({id: data._id, isAdmin: data.isAdmin})
	// 		console.log(data._id);
	// 	})
	// };

	useEffect(()=> {
		console.log(productId);
		fetch(`${process.env.REACT_APP_URL}/products/${productId}`)
		.then(res=> res.json())
		.then(data=> {
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	},[productId]);

	const buy = (productId, productQuantity, productPrice)=> {
		fetch(`${process.env.REACT_APP_URL}/users/checkout`, {
			method: 'POST',
			headers: {
				"Content-Type":"application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				quantity: 1,
				price: price
			})
		})
		.then(res=> res.json())
		.then(data=> {
			console.log(data)
			if(data.order !== null) {
				console.log(data)
				Swal.fire({
					title: "Item bought.",
					icon: "success",
					text: "Thank you for ordering!"
				})
				navigate('/products')
			} else {
				Swal.fire({
					title: "Something went wrong!",
					icon: "error",
					text: "Check your credentials!"
				})
			}
		})
	};

	return(
		<Container className="my-3">
			<Row>
				<Col>
					
					<Card>
						<Card.Body>
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>{price}</Card.Text>

							{
								(user.id !== null)
								?
								<Button
									variant="primary"
									onClick={()=> buy(productId)}>
										Order
								</Button>
								:
								<>
								<Link
									className="btn btn-danger"
									to="/login">
										Log in to Buy
								</Link>
								<Button
									as={Link}
									to={'/products'}>
										Products
								</Button>
								</>
							}
							

						</Card.Body>
					</Card>

				</Col>
			</Row>
		</Container>
	)
};