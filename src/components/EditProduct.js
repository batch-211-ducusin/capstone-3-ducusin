import { Navigate, Link, useNavigate, useParams } from 'react-router-dom';
import { useContext, useState, useEffect } from 'react';
import { Form, Button } from 'react-bootstrap';
import UserContext from '../UserContext.js';
import Swal from 'sweetalert2';

export default function EditProduct() {

	const { user } = useContext(UserContext);
	const { productId } = useParams();
	const navigate = useNavigate();

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [isActive, setIsActive] = useState(false);

	function editProduct(e) {
		// e.preventDefault();
		fetch(`${process.env.REACT_APP_URL}/products/editProduct/${productId}`, {
			method: 'PUT',
			headers: {
				"Content-Type":"application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
			})
		})
		.then(res=> res.json())
		.then(data=> {
			console.log(data);
			if(data) {
				Swal.fire({
					title: "Product successfully updated.",
					icon: "success",
					text: `${name} is now updated.`
				});
				navigate("adminDash");
			} else {
				Swal.fire({
					title: "Error!",
					icon: "error",
					text: "Somthing went wrong. Please try again later!"
				});
			}
		})
		// setName('');
		// setDescription('');
		// setPrice('');
	}

	useEffect(()=> {
		if(
			name !== "" &&
			description !== "" &&
			price > 0
		) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	},[name, description, price]);

	useEffect(()=> {
		console.log(productId);
		fetch(`${process.env.REACT_APP_URL}/products/${productId}`)
		.then(res=> res.json())
		.then(data=> {
			console.log(data);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	},[productId]);

	return(
		(user.isAdmin === true)
		?
		<>
		<h1>Edit Product</h1>
		<Form onSubmit={(e)=> editProduct(e)}>

			<Form.Group>
				<Form.Label>
					Product Name
				</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter product name"
					value={name}
					onChange={e=> setName(e.target.value)}
					required>
				</Form.Control>
			</Form.Group>

			<Form.Group>
				<Form.Label>
					Product Description
				</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter product description"
					value={description}
					onChange={e=> setDescription(e.target.value)}
					required>
				</Form.Control>
			</Form.Group>

			<Form.Group className="mb-3">
				<Form.Label>
					Product Price
				</Form.Label>
				<Form.Control
					type="number"
					placeholder="Enter product price"
					value={price}
					onChange={e=> setPrice(e.target.value)}
					required>
				</Form.Control>
			</Form.Group>

			{
			(isActive)
			?
			<Button
				className="me-1 mb-1"
				variant="primary"
				type="submit"
				id="submitBtn">
					Update
			</Button>
			:
			<Button
				className="me-1 mb-1"
				variant="danger"
				type="submit"
				id="submitBtn">
					Update
			</Button>
			}
			<Button
				className="mb-1"
				variant="primary"
				type="submit"
				id="submitBtn"
				as={Link}
				to="/adminDash">
					Cancel
			</Button>
		
		</Form>
		</>
		:
		<Navigate to="/adminDash" />
	)
};