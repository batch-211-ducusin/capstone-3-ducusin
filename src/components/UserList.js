import { useState, useEffect } from 'react';
import { Table, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function UserList() {

	const [allUsers, setAllUsers] = useState([]);

	const fetchUserData = ()=> {
		fetch(`${process.env.REACT_APP_URL}/users/all`, {
			headers: {"Authorization": `Bearer ${localStorage.getItem('token')}`}
		})
		.then(res=> res.json())
		.then(data=> {
			console.log(data);
			setAllUsers(data.map(user=> {
				return(
					<tr>
						<td>{user._id}</td>
						<td>{user.firstName}</td>
						<td>{user.lastName}</td>
						<td>{user.email}</td>
						<td>{user.mobileNo}</td>
						<td>{user.isAdmin ? "Admin" : "User"}</td>
						{
							(user.isAdmin === true)
							?
							<Button
								variant="success"
								size="sm"
								onClick={()=> setAsUser(user._id, user.email)}
								>
									Set as User
							</Button>
							:
							<Button
								variant="danger"
								size="sm"
								onClick={()=> setAsAdmin(user._id, user.email)}
								>
									Set as Admin
							</Button>
						}
					</tr>
				)
			}))
		})
	};

	useEffect(()=> {
		fetchUserData();
	})

	const setAsAdmin = (userId, userEmail)=> {
		console.log(userId);
		console.log(userEmail);
		fetch(`${process.env.REACT_APP_URL}/users/setToAdmin/${userId}`, {
			method: 'PUT',
			headers: {
				"Content-Type":"application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({isAdmin:true})
		})
		.then(res=> res.json())
		.then(data=> {
			console.log(data);
			if(data) {
				Swal.fire({
					title: "Successful!",
					icon: "success",
					text: `${userEmail} is now an Admin.`
				})
				fetchUserData();
			} else {
				Swal.fire({
					title: "Unsuccessful!",
					icon: "error",
					text: "Something went wrong. Please try again later."
				})
			}
		})
	};

	const setAsUser = (userId, userEmail)=> {
		console.log(userId);
		console.log(userEmail);
		fetch(`${process.env.REACT_APP_URL}/users/setToUser/${userId}`, {
			method: 'PUT',
			headers: {
				"Content-Type":"application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({isAdmin:false})
		})
		.then(res=> res.json())
		.then(data=> {
			console.log(data);
			if(data) {
				Swal.fire({
					title: "Successful!",
					icon: "success",
					text: `${userEmail} is now a User.`
				})
				fetchUserData();
			} else {
				Swal.fire({
					title: "Unsuccessful!",
					icon: "error",
					text: "Something went wrong. Please try again later."
				})
			}
		})
	};

	return(
		<>
		<h1>User List</h1>
		<div className="table-responsive">
		<Table className="table">
			<thead>
				<tr>
					<th>ID</th>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Email</th>
					<th>Mobile Number</th>
					<th>Status</th>
				</tr>
			</thead>
			<tbody>
				{allUsers}
			</tbody>
		</Table>
		</div>
		</>
	)
};