import {Row, Col, Card} from 'react-bootstrap';

export default function Highlights() {
	return (

		<Row className="mt-3 mb-3">

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
      				{/*<Card.Img variant="top" src="holder.js/100px180" />*/}
      				<Card.Body>
        				<Card.Title>Hassle free ordering</Card.Title>
       				 	<Card.Text>
          					As easy as 1, 2, 3 you can have an amazing party!
        				</Card.Text>
      				</Card.Body>
    			</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
      				{/*<Card.Img variant="top" src="holder.js/100px180" />*/}
      				<Card.Body>
        				<Card.Title>Sit back, relax, enjoy!</Card.Title>
       				 	<Card.Text>
          					Why wash the dishes when you can have it ordered?
        				</Card.Text>
      				</Card.Body>
    			</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
      				{/*<Card.Img variant="top" src="holder.js/100px180" />*/}
      				<Card.Body>
        				<Card.Title>Pure fun! less worries!</Card.Title>
       				 	<Card.Text>
          					Late night party will never be the same again!
        				</Card.Text>
      				</Card.Body>
    			</Card>
			</Col>
		</Row>

	)
}