import { useContext, useState, useEffect } from 'react';
import { Navigate, Link } from 'react-router-dom';
import { Table, Button } from 'react-bootstrap';
import UserContext from '../UserContext.js';
import Swal from 'sweetalert2';

export default function AdminDash() {

	const { user } = useContext(UserContext);
	const [allProducts, setAllProducts] = useState([]);

	const fetchData = ()=> {
		fetch(`${process.env.REACT_APP_URL}/products/all`, {
			headers: {"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res=> res.json())
		.then(data=> {
			console.log(data);
			setAllProducts(data.map(product=> {
				return(
					<tr>
						<td>{product._id}</td>
						<td>{product.name}</td>
						<td>{product.description}</td>
						<td>{product.price}</td>
						<td>{product.isActive ? "Active" : "Inactive"}</td>
						{
							(product.isActive)
							?
							<Button
								variant="danger"
								size="sm"
								onClick={()=> archive(product._id, product.name)}
								>
									Deactivate
							</Button>
							:
							<>
							<Button
								variant="success"
								size="sm"
								onClick={()=> unarchive(product._id, product.name)}
								>
									Activate
							</Button>
							<Button
								as={Link}
								to={`/editProduct/${product._id}`}
								variant="secondary"
								size="sm"
								className="m-2">
									Edit
							</Button>
							</>
						}
					</tr>
				)
			}))
		})
	}

	useEffect(()=> {
		fetchData();
	})

	const archive = (productId, productName)=> {
		console.log(productId);
		console.log(productName);
		fetch(`${process.env.REACT_APP_URL}/products/archive/${productId}`, {
			method: 'PUT',
			headers: {
				"Content-Type":"application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({isActive:false})
		})
		.then(res=> res.json())
		.then(data=> {
			console.log(data);
			if(data) {
				Swal.fire({
					title: "Archive Successful!",
					icon: "success",
					text: `${productName} is now inactive.`
				})
				fetchData();
			} else {
				Swal.fire({
					title: "Archive Unsuccessful!",
					icon: "error",
					text: "Something went wrong. Please try again later."
				})
			}
		})
	}

	const unarchive = (productId, productName)=> {
		console.log(productId);
		console.log(productName);
		fetch(`${process.env.REACT_APP_URL}/products/activate/${productId}`, {
			method: 'PUT',
			headers: {
				"Content-type":"application/json",
				Authorization:`Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({isActive: true})
		})
		.then(res=> res.json())
		.then(data=> {
			console.log(data);
			if(data) {
				Swal.fire({
					title: "Unarchive Succesful!",
					icon: "success",
					text: `${productName} is now active.`
				})
				fetchData();
			} else {
				Swal.fire({
					title: "Unarchive Unseccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later.`
				})
			}
		})
	}

	return(
		(user.isAdmin === true)
		?
		<>
		<div>
			<h1>Admin Dashboard</h1>
			<Button
				as={Link}
				to="/addProduct"
				variant="primary"
				size="md"
				className="mx-2 my-2" >
					Add Product
			</Button>
			<Button
				as={Link}
				to="/userList"
				variant="warning"
				size="md"
				className="mx-2 my-2" >
					Show All Users and Admins
			</Button>
		</div>
		<div className="table-responsive">
		<Table className="table">
			<thead>
				<tr>
					<th>Product ID</th>
					<th>Product Name</th>
					<th>Description</th>
					<th>Price</th>
					<th>Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				{allProducts}
			</tbody>
		</Table>
		</div>
		</>
		:
		<Navigate to="/products" />
	)
};